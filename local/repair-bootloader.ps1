#!/usr/bin/env pwsh
# This script demonstrates the process for repairing a broken bootloader.
function repair-bootloader ([string]$diskpart) {
    cmd /c diskpart /s $diskpart # mark the windows partition as active so it can be booted from
    bootrec /fixmbr
    bootsect /nt60 all /force
    attrib -h -s c:\boot\bcd
    del c:\boot\bcd # delete current bootloader
    bcdedit /createstore c:\boot\bcd
    bcdedit /store c:\boot\bcd /create {bootmgr} /d "Windows Boot Manager"
    bcdedit /store c:\boot\bcd /set {bootmgr} device partition=C:
    bcdedit /store c:\boot\bcd /timeout 10
    bcdedit /store c:\boot\bcd /create /d "Microsoft Windows" /application osloader # this command will return the GUID used in the next 5 commands
    bcdedit /store c:\boot\bcd /set {GUID} device partition=C:
    bcdedit /store c:\boot\bcd /set {GUID} osdevice partition=C:
    bcdedit /store c:\boot\bcd /set {GUID} path \Windows\system32\winload.exe
    bcdedit /store c:\boot\bcd /set {GUID} systemroot \Windows
    bcdedit /store c:\boot\bcd /displayorder {GUID}
    bcdedit /import c:\boot\bcd
}

$example_diskpart = 'C:\repair-bootloader.txt'

repair-bootloader $example_diskpart
