#!/usr/bin/env pwsh
# This script will modify all users within a list of departments to have consistent information.
Import-Module ActiveDirectory

function modify-ad-user ([array]$departments) {
    $tld = 'com'
    $company = 'vandelayindustries'
    $upnsuffix = "$company.$tld"
    $server = "dc1.internal.$company.$tld"
    $domain = "DC=internal,DC=$company,DC=$tld"
    $continent = 'europe'
    $country = 'germany'
    $city = 'berlin'

    foreach ($department in $departments) {
        $ou = "OU=$department,OU=$city,OU=$country,OU=$continent,OU=users,OU=$company,$domain"
        $users = get-aduser -filter * -searchbase $ou -server $server
        foreach ($user in $users) {
	    $givenname = $user.givenname.tolower()
            $surname = $user.surname.tolower()
	    $sam = $givenname[0] + $surname
            $upn = "$sam@$upnsuffix"
            $email = "$givenname.$surname@$upnsuffix"
	    $newname = $user.givenname + ' ' + $user.surname
	    $proxies = @("SMTP:$email", "smtp:$upn")
            set-aduser -identity $user -replace @{Mail = $email;ProxyAddresses = $proxies;sAMAccountName = $sam;UserPrincipalName = $upn}
	    rename-adobject -identity $user -newname $newname
        }
    }
}

$example_departments = @('finance','human-resources','it-administration','logistics','software-development')

modify-ad-user $example_departments
