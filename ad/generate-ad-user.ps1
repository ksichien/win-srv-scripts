#!/usr/bin/env pwsh
# This script was used to create new user accounts based on a CSV file.
Import-Module ActiveDirectory

function generate-ad-user ([string]$csv) {
    $tld = 'com'
    $company = 'vandelayindustries'
    $upnsuffix = "$company.$tld"
    $server = "dc1.internal.$company.$tld"
    $domain = "DC=internal,DC=$company,DC=$tld"
    $password = convertto-securestring 'P@ssword!' -asplaintext -force
    $users = import-csv -path $csv
    foreach ($user in $users) {
        $email = $user.'Email'.tolower()
        $fullsam = $email -replace "@$upnsuffix"
        $firstname = $fullsam.split('.')[0].tolower()
        $lastname = $fullsam.split('.')[-1].tolower()
        $sam = $firstname[0] + $lastname
	$displayname = $user.'Name'
        $upn = "$fullsam@$upnsuffix"
        $ou = "OU=temporary,OU=users,OU=$company,$domain"
        $aduser = get-aduser -filter {sAMAccountName -eq $sam} # check if user already exists
        $fulladuser = get-aduser -filter {sAMAccountName -eq $fullsam} # check if user already exists
        if ($aduser -eq $null -and $fulladuser -eq $null) {
            if ($fullsam.length -ge 21) {
                $sam = $firstname[0] + $lastname
            }
            else {
                $sam = $fullsam
            }
            new-aduser -Name $sam -DisplayName $displayname `
              -GivenName (get-culture).textinfo.totitlecase($firstname) `
              -Surname (get-culture).textinfo.totitlecase($lastname) `
              -Path $ou -AccountPassword $password -ChangePasswordAtLogon $True `
              -Enabled $True -UserPrincipalName $upn -Server $server
            $newaduser = get-aduser -filter {sAMAccountName -eq $sam} -searchbase $ou
            set-aduser -Identity $newaduser -replace @{mail = $email} -Server $server
            rename-adobject -identity $newaduser -newname $displayname
        }
    }
}

$example_csv = 'C:\staff.csv'

generate-ad-user $example_csv
