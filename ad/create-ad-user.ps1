#!/usr/bin/env pwsh
# This is a script to interactively create new user accounts.
Import-Module ActiveDirectory

function create-ad-user ([string]$firstname, [string]$lastname, [string]$department) {
    $tld = 'com'
    $company = 'vandelayindustries'
    $upnsuffix = "$company.$tld"
    $server = "dc1.internal.$company.$tld"
    $domain = "DC=internal,DC=$company,DC=$tld"
    $continent = 'europe'
    $country = 'germany'
    $city = 'berlin'
    
    $password = ConvertTo-SecureString 'P@ssword!' -AsPlainText -Force

    $username = $firstname[0].ToLower() + $lastname.ToLower()
    $path = "OU=$department,OU=$city,OU=$country,OU=$continent,OU=users,OU=$company,$domain"

    Write-Output "Creating user $firstname $lastname as $username in $department, located in $path."
    $confirm = Read-Host -Prompt 'Continue? (y/n)'

    if ($confirm -eq 'y') {
        New-ADUser -DisplayName "$firstname $lastname" -GivenName "$firstname" `
	  -Name "$username" -Path $path -SamAccountName $username `
	  -Server $server -Surname "$lastname" -Type 'user' `
	  -UserPrincipalName "$username@$upnsuffix"
	$identity = Get-ADUser -filter {sAMAccountName -eq $username}
        Set-ADAccountPassword -Identity $identity -NewPassword $password `
	  -Reset $true -Server $server
        Enable-ADAccount -Identity $identity -Server $server
        Add-ADPrincipalGroupMembership -Identity $identity `
	  -MemberOf "CN=$department,OU=groups,OU=$company,$domain" -Server $server
        Set-ADAccountControl -AccountNotDelegated $false -AllowReversiblePasswordEncryption $false `
	  -CannotChangePassword $false -DoesNotRequirePreAuth $false -Identity $identity `
	  -PasswordNeverExpires $false -Server $server -UseDESKeyOnly $false
        Set-ADUser -ChangePasswordAtLogon $true -Identity $identity -Server $server `
	  -SmartcardLogonRequired $false
    }
}

do {
    Write-Output 'Please provide the following information for the new user:'
    $firstname = Read-Host -Prompt 'First name'
    $first = $firstname.SubString(0, 1).ToUpper() + $firstname.SubString(1).ToLower()
    $lastname = Read-Host -Prompt 'Last name'
    $last = $lastname.SubString(0, 1).ToUpper() + $lastname.SubString(1).ToLower()
    $department = Read-Host -Prompt 'Department'
    $dpt = $department.ToLower()

    create-ad-user $first $last $dpt

    $continue = Read-Host -Prompt 'Would you like to add another user? (y/n)'
} while ($continue -eq 'y')
