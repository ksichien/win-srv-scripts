#!/usr/bin/env pwsh

function generate-ad-password ([int]$amount) {
    $uc = 2 # number of uppercase characters
    $dc = 2 # number of digits
    $sc = 2 # number of special characters
    $lc = ($amount - ($uc + $dc + $sc)) # number of lowercase characters

    $uppers = -join ((65..90) | Get-Random -Count $uc | foreach {[char]$_})
    $lowers = -join ((97..122) | Get-Random -Count $lc | foreach {[char]$_})
    $digits = -join ((48..57) | Get-Random -Count $dc | foreach {[char]$_})
    $specials = -join ((35..38) + (63..64) | Get-Random -Count $sc | foreach {[char]$_})
    return -join (($uppers + $lowers + $digits + $specials).tochararray() | sort-object { Get-Random })
}

generate-ad-password 16
