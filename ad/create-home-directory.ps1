#!/usr/bin/env pwsh
# This script will create and assign home directories for all users within a list of departments.
Import-Module ActiveDirectory

function create-home-directory ([array]$departments) {
    $tld = 'com'
    $company = 'vandelayindustries'
    $upnsuffix = "$company.$tld"
    $server = "dc1.internal.$company.$tld"
    $domain = "DC=internal,DC=$company,DC=$tld"
    $dfs = '\\internal.' + "$company.$tld" + '\shares'
    $continent = 'europe'
    $country = 'germany'
    $city = 'berlin'
    $homedrive = 'U:'

    foreach ($department in $departments) {
        $ou = "OU=$department,OU=$city,OU=$country,OU=$continent,OU=users,OU=$company,$domain"
        $users = get-aduser -filter * -searchbase $ou -server $server
        foreach ($user in $users) {
            $username = $user.samAccountName
            $homedirectory = "$dfs\home\$username"
            if (-not(test-path -path "$homedirectory")) {
                new-item $homedirectory -type directory
                $acl = get-acl $homedirectory
                $ar = new-object System.Security.AccessControl.FileSystemAccessRule($username, 'Modify', 'ContainerInherit,ObjectInherit', 'None', 'Allow')
                $acl.setaccessrule($ar)
                set-acl $homedirectory $acl
            }
            set-aduser -identity $user -replace @{HomeDirectory = "$homedirectory"; HomeDrive = "$homedrive"}
        }
    }
}

$example_departments = @('finance','human-resources','it-administration','logistics','software-development')

create-home-directory $example_departments
