#!/usr/bin/env pwsh
# This script will list all computer and user accounts that haven't logged on within the past 3 months.
Import-Module ActiveDirectory

function list-inactive-objects ([string]$company, [string]$domain) {
    $datecutoff = (Get-Date).AddDays(-90)
    $searchbasecomputers = "OU=computers,OU=$company,$domain"
    $searchbaseusers = "OU=users,OU=$company,$domain"

    Write-Output 'Computers:'
    Get-ADComputer -SearchBase $searchbasecomputers -Properties LastLogonDate -Filter {LastLogonDate -lt $datecutoff} | sort-object LastLogonDate | ft Name, LastLogonDate -Autosize
    Write-Output 'Users:'
    Get-ADUser -SearchBase $searchbaseusers -Properties LastLogonDate -Filter {LastLogonDate -lt $datecutoff} | sort-object LastLogonDate | ft Name, LastLogonDate -Autosize
}

$example_company = 'vandelayindustries'
$example_domain = 'DC=internal,DC=vandelayindustries,DC=com'

list-inactive-objects $example_company $example_domain
