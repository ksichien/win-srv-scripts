#!/usr/bin/env pwsh
# This script was used to create new user accounts and migrate existing ones after restructuring an existing directory tree.
# All data was provided by the HR department in a CSV file with several typos and inconsistencies, so the script includes some error handling as well.
Import-Module ActiveDirectory

function migrate-ad-user ([string]$csv) {
    $tld = 'com'
    $company = 'vandelayindustries'
    $upnsuffix = "$company.$tld"
    $server = "dc1.internal.$company.$tld"
    $domain = "DC=internal,DC=$company,DC=$tld"
    $continent = 'europe'
    $country = 'germany'
    $codes = @{
        BB = 'brandenburg'
        BE = 'berlin'
	BLN = 'berlin'
        BW = 'baden-wurttemberg'
        BY = 'bavaria'
        HB = 'bremen'
        HE = 'hesse'
        HH = 'hamburg'
        MV = 'mecklenburg-vorpommern'
        NI = 'lower saxony'
        NV = 'north rhine-westphalia'
        RP = 'rhineland-palatinate'
        SH = 'schleswig-holstein'
        SL = 'saarland'
        SN = 'saxony'
        ST = 'saxony-anhalt'
        TH = 'thuringia'
    }
    $password = convertto-securestring 'P@ssword!' -asplaintext -force
    $users = import-csv -path $csv
    foreach ($user in $users) {
        $employee = $user.'Employee Name'.tolower() -replace "'" # downcase and remove quote symbols
        $firstname = $employee.split(' ')[0].tolower()
        $lastname = $employee.split(' ')[-1].tolower()
        if ($employee.split(' ').length -ge 2) { # include middle name if possible
            $middlename = $employee.split(' ')[1].tolower()
            $displayname = (get-culture).textinfo.totitlecase($firstname) + ' ' + `
	      (get-culture).textinfo.totitlecase($middlename) + ' ' + `
              (get-culture).textinfo.totitlecase($lastname)
            $sam = $firstname[0] + $middlename[0] + $lastname
        }
        else {
            $displayname = (get-culture).textinfo.totitlecase($firstname) + ' ' + `
              (get-culture).textinfo.totitlecase($lastname)
      	    $sam = $firstname[0] + $lastname
        }
        $upn = "$sam@$upnsuffix"
        $title = $user.Title
        $department = $user.Department.tolower()
        $city = -join $user.'Employee Code'[2..4]
        if ($city -match '[0-9]') {
      	    $city = -join $city[0..1] # trim the city code in case of inconsistencies in the csv file
        }
        if ($codes[$city] -eq $null) {
      	    $city = 'BE' # error handling in case of absent city code
        }
        $ou = "OU=$department,OU=$($codes[$city]),OU=$country,OU=$continent,OU=users,OU=$company,$domain"
        $aduser = get-aduser -filter {sAMAccountName -eq $sam} # check if user already exists
        if ($aduser -eq $null) {
      	    new-aduser -Name $sam -DisplayName $displayname `
	    -GivenName (get-culture).textinfo.totitlecase($firstname) `
            -Surname (get-culture).textinfo.totitlecase($lastname) -Path $ou `
            -AccountPassword $password -ChangePasswordAtLogon $True -Enabled $True `
            -UserPrincipalName $upn -Title $title -Department $department -Server $server
        }
        else {
      	    set-aduser -Identity $aduser -DisplayName $displayname -Title $title `
	      -Department $department -Server $server
      	    move-adobject -Identity $aduser -TargetPath $ou -Server $server
        }
    }
}

$example_csv = 'C:\migrate-ad-user.csv'

migrate-ad-user $example_csv
