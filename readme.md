# Windows Server Scripts
This is a small collection of PowerShell scripts I've written to make life easier for myself.

## Overview

- ad:
    - create-ad-user
    - create-home-directory
    - generate-ad-user
    - generate-ad-password
    - list-active-adapters
    - list-inactive-objects
    - migrate-ad-user
    - modify-ad-user
- licensing:
    - install-product-key
- local:
    - create-local-user
    - create-scheduled-task
    - create-vm
    - format-disk
    - repair-bootloader
- sql:
    - sql-backup
- wsus:
    - mail-wsus-report
    - wsus-force-check
